**Israel Molina Ferrer** **13/02/2014**
=======

=======

**Caracteristicas:** 
======================

La pagina esta formada por enlaces y referencia a imagenes.
Dividida la pagina principal en tres partes, el usuario interactua con la parte central, que es la principal, y en la que aparece la informacion.
Para que todo funcione correctamente he localizado las imagenes en una carpeta llamada iamgenes, que esta metida dentro de otra carpeta llamada html, 
donde estan localizadas las paginas anexas donde se dirige al usuario desde la parte central del inicio.

**Conclusiones:**
=================


La practica es interesante y educativa, pero veo necesario mas seguimiento en clase, para apoyar y reforzar lo aprendido en la practica, 
por que debido a mi corta experiencia informatica, no puede encontrar las etiquetas y atributos necesarios para realizar un trabajo mejor,
que con una breve explicacion podria haberse solucionado.